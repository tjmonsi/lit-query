```js script
import { html } from '@open-wc/demoing-storybook';
import '../lit-query.js';

export default {
  title: 'LitQuery',
  component: 'lit-query',
  options: { selectedPanel: "storybookjs/knobs/panel" },
};
```

# LitQuery

A component for...

## Features:

- a
- b
- ...

## How to use

### Installation

```bash
yarn add lit-query
```

```js
import 'lit-query/lit-query.js';
```

```js preview-story
export const Simple = () => html`
  <lit-query></lit-query>
`;
```

## Variations

###### Custom Title

```js preview-story
export const CustomTitle = () => html`
  <lit-query title="Hello World"></lit-query>
`;
```
