# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.3](https://gitlab.com/tjmonsi/lit-query/compare/v0.0.2...v0.0.3) (2020-08-30)


### Bug Fixes

* fix on saving and reading from property ([6940dcc](https://gitlab.com/tjmonsi/lit-query/commit/6940dcce7c614feca77283d789f446f625c53e00))

### [0.0.2](https://gitlab.com/tjmonsi/lit-query/compare/v0.0.1...v0.0.2) (2020-08-30)


### Bug Fixes

* fix for fail on class decorators ([82f231e](https://gitlab.com/tjmonsi/lit-query/commit/82f231e16f98114fb50df90bca653558b9a0ad0f))
* fix for fail on class decorators ([1934732](https://gitlab.com/tjmonsi/lit-query/commit/193473282e96d8cd177f92f6715edcfbb84ce94e))

### 0.0.1 (2020-08-30)


### Features

* add lit-query ([f2f63ac](https://gitlab.com/tjmonsi/lit-query/commit/f2f63ace652729e488d36584ad75f6fdf9ee6099))
